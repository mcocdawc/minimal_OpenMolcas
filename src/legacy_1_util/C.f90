module C_mod
    use B_mod, only: f_B
    implicit none
    private
    public :: f_C

    contains

    integer function f_C()
        f_C = f_B() + 1
    end function
end module
