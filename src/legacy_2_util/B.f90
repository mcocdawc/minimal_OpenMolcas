module B_mod
    use A_mod, only: f_A
    implicit none
    private
    public :: f_B

    contains

    integer function f_B()
        f_B = f_A() + 1
    end function
end module
