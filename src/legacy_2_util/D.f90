module D_mod
    use C_mod, only: f_C
    implicit none
    private
    public :: f_D

    contains

    integer function f_D()
        f_D = f_C() + 1
    end function
end module
