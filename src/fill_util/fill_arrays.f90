module fill_arrays
    implicit none
    private
    public :: fill
contains
    elemental subroutine fill(X, val)
        real, intent(inout) :: X
        real, intent(in) :: val
        X = val
    end subroutine
end module
