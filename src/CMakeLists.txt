add_compile_options(-Wall -Wextra -Werror)

append_absolute_paths(mods ${CMAKE_CURRENT_LIST_DIR}/legacy_1_util
    A.F90 C.f90)

append_absolute_paths(mods ${CMAKE_CURRENT_LIST_DIR}/legacy_2_util
    B.f90 D.f90)

bundle_library_with_modules(libmolcas "${mods}"
    legacy_1_util
    legacy_2_util
)

set_target_properties(libmolcas PROPERTIES PREFIX "")

add_subdirectory(scf/)

add_subdirectory(rasscf/)
