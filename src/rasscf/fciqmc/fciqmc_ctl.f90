module fciqmc
    implicit none
    private
    public :: fciqmc_ctl

contains

    real pure function fciqmc_ctl(x)
        real, intent(in) :: x
        fciqmc_ctl = 2.0 * x
    end function

end module
