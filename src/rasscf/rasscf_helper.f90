module rasscf_helper
    use fill_arrays, only: fill
    implicit none
    private
    public :: set_5
contains

    elemental subroutine set_5(X)
        real, intent(inout) :: X
        call fill(X, 5.0)
    end subroutine

end module
