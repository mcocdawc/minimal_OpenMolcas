program rasscf
    use zero_arrays, only: set_zero
    use rasscf_helper, only: set_5
    use fciqmc, only: fciqmc_ctl
    use D_mod, only: f_D
    implicit none
    abstract interface
        real pure function CI_solver_t(x)
            real, intent(in) :: x
        end function
    end interface

    procedure(CI_solver_t), pointer :: CI_solver

    real :: X(2)


    write(*, *) f_D()

    X = [3, 4]
    write(*, *) X
    call set_zero(X)
    write(*, *) X

    write(*, *) X
    call set_5(X)
    write(*, *) X

    CI_solver => fciqmc_ctl
    write(*, *) CI_solver(3.)
end program
