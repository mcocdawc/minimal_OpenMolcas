module zero_arrays
    use fill_arrays, only: fill
    implicit none
    private
    public :: set_zero
contains

    elemental subroutine set_zero(X)
        real, intent(inout) :: X
        call fill(X, 0.0)
    end subroutine

end module
