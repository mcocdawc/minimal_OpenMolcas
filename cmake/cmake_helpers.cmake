cmake_minimum_required(VERSION 3.13) # needed for the FILTER generator expression
cmake_policy(SET CMP0079 NEW)


function(add_Fortran_library Target)
# Has the same signature as add_library, but adds the
# Fortran_MODULE_DIRECTORY property in addition.
    add_library(${ARGV})
    set(MOD_DIR ${PROJECT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/mod/${Target})
    set_target_properties(${Target} PROPERTIES Fortran_MODULE_DIRECTORY ${MOD_DIR})
    target_include_directories(${Target} INTERFACE ${MOD_DIR})
endfunction()

function(add_part_of_libmolcas Target)
    set(sources "")
    append_absolute_paths(sources ${CMAKE_CURRENT_LIST_DIR}
        ${ARGN})
    add_Fortran_library(${Target} EXCLUDE_FROM_ALL OBJECT ${sources})
endfunction()

function(add_directory Dir)
    get_filename_component(base_name ${Dir} NAME)
    add_subdirectory(${Dir} ${base_name}_bin)
endfunction()

function(pass_properties_to_files Target Files Properties)
    foreach(prop ${Properties})
        set_source_files_properties(${Files} PROPERTIES ${prop} $<TARGET_PROPERTY:${Target},${prop}>)
    endforeach()
endfunction()

function(target_remove_sources Target)
    get_target_property(sources ${Target} SOURCES)
    list(REMOVE_ITEM sources ${ARGN})
    set_target_properties(${Target} PROPERTIES SOURCES ${sources})
endfunction()

function(bundle_library_with_modules Target ModuleFiles)
    add_Fortran_library(${Target}_modules EXCLUDE_FROM_ALL OBJECT ${ModuleFiles})
    add_Fortran_library(${Target} $<TARGET_OBJECTS:${Target}_modules>)
    target_include_directories(${Target} INTERFACE $<TARGET_PROPERTY:${Target}_modules,Fortran_MODULE_DIRECTORY>)
    foreach(lib ${ARGN})
        add_subdirectory(${lib})
        get_target_property(dependencies ${lib} LINK_LIBRARIES)
        if(dependencies)
            add_dependencies(${Target}_modules ${dependencies})
        endif()
        target_link_libraries(${lib} ${Target}_modules)
        get_target_property(sources ${lib} SOURCES)
        set(mods_in_lib "")
        foreach(mod ${ModuleFiles})
            if(${mod} IN_LIST sources)
                list(APPEND mods_in_lib ${mod})
            endif()
        endforeach()
        target_sources(${Target} PRIVATE $<TARGET_OBJECTS:${lib}>)
        target_remove_sources(${lib} ${mods_in_lib})
        pass_properties_to_files(${lib} "${mods_in_lib}" "COMPILE_DEFINITIONS;INCLUDE_DIRECTORIES")
    endforeach()
endfunction()


function (append_absolute_paths rel_path_sources base_dir)
    set(local_sources ${${rel_path_sources}})

    foreach (file ${ARGN})
        get_filename_component(file_path "${file}" REALPATH BASE_DIR "${base_dir}")
        if (NOT EXISTS ${file_path})
            message(FATAL_ERROR "${file_path} does not exist")
        endif()
        list(APPEND local_sources ${file_path})
    endforeach()
    set(${rel_path_sources} ${local_sources} PARENT_SCOPE)
endfunction()
